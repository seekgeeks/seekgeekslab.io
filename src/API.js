import axios from 'axios';
import isEmpty from 'lodash/isEmpty';

// export const rootURL = 'http://localhost:8000'
// export const rootURL = 'https://morning-fjord-79256.herokuapp.com'
export const rootURL = 'https://api.seekgeeks.co'
export const AuthURL = `${rootURL}/api/v1/auth/login/`

const getAuthenticationHeader = () => {
  return {
    headers: {
      Authorization: 'JWT ' + localStorage.getItem('token')
    }
  }
}

const multiPartHeader = () => {
  return {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'JWT ' + localStorage.token
    }
  }
}

export const profile = (payload) => {
  return userInfo()
    .then(res => res.data.profile)
    .then(profileId => axios.patch(`${rootURL}/profiles/${profileId}/`, payload, getAuthenticationHeader()))
}

export const userInfo = () => {
  console.log(localStorage.getItem('token'));
  return axios.get(rootURL + '/api/v1/auth/user-info/', getAuthenticationHeader())
}

export const profileStrength = () => {
  return axios.get(rootURL + '/profile_strength/', getAuthenticationHeader())
}

export const profileDetail = () => {
  return userInfo()
    .then(res => res.data.profile)
    .then(profileId => axios.get(`${rootURL}/profiles/${profileId}/`, getAuthenticationHeader()))
}

export const getCategories = () => {
  return axios.get(`${rootURL}/requirement/categories/`)
}

export const createDocument = (payload) => {
  return axios
    .post(`${rootURL}/requirement/documents/`, payload, multiPartHeader())
}

export const documents = (payload) => {
  return axios.get(`${rootURL}/requirement/documents/?id__in=${payload.join()}`, getAuthenticationHeader())
}

export const deleteDocument = (id) => {
  return axios.delete(`${(rootURL)}/requirement/documents/${id}/`)
}

export const freelancerDetail = () => {
  return userInfo()
    .then(res => res.data.freelancer)
    .then(freelancerId => axios.get(`${rootURL}/freelancers/${freelancerId}/`, getAuthenticationHeader()))
    .then(res => {
      if (isEmpty(res.data.documents)) {
        return res
      } else {
        return documents(res.data.documents)
          .then(resDoc => {
            res.data.documents = resDoc.data
            return res
          })
      }
    })
}

export const freelancer = (payload) => {
  return userInfo()
    .then(res => res.data.freelancer)
    .then(freelancerId => axios.patch(`${rootURL}/freelancers/${freelancerId}/`, payload, getAuthenticationHeader()))
}

export const vouch = (payload) => {
  const vouch1 = {
    name: payload.name1,
    email: payload.email1,
    company: payload.company1,
    position: payload.position1,
  }
  const vouch2 = {
    name: payload.name2,
    email: payload.email2,
    company: payload.company2,
    position: payload.position2,
  }
  if (payload.vouch1) {
    axios.patch(`${rootURL}/vouches/${payload.vouch1}/`, vouch1, getAuthenticationHeader())
  }

  if (payload.vouch2) {
    axios.patch(`${rootURL}/vouches/${payload.vouch2}/`, vouch2, getAuthenticationHeader())
  }

  return freelancerDetail()
    .then(res => {
      if (res.data.vouches.length < 2) {
        var vouches = res.data.vouches

        if (vouches.length === 0) {
          return axios
            .post(`${rootURL}/vouches/`, vouch1, getAuthenticationHeader())
            .then(res => {
              vouches.push(res.data.id)
              return axios
                .post(`${rootURL}/vouches/`, vouch2, getAuthenticationHeader())
                .then(res => {
                  vouches.push(res.data.id)
                  return freelancer({ vouches })
                })
            })
        } else {
          return axios
            .post(`${rootURL}/vouches/`, vouch2, getAuthenticationHeader())
            .then(res => {
              vouches.push(res.data.id)
              return freelancer({ vouches })
            })
        }
        
      }
    })
}

export const vouchDetail = (id) => {
  return axios.get(`${rootURL}/vouches/${id}/`, getAuthenticationHeader())
}


export const createContact = (payload) => {
  return axios
    .post(`${(rootURL)}/requirement/contacts/`, payload)
}

export const requirement = (payload) => {
  return axios.post(`${(rootURL)}/requirement/requirements/`, payload)
}