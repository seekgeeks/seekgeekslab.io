import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom'

import {loggedIn} from './Auth'

import NotFound from './components/NotFound'
import withTracker from './components/withTracker';
import Contact from './components/Contact'
import Requirement from './components/Requirement'
import Page from './components/Page'
import Home from './components/Home'
import Partner from './components/Partner'
import Login from './components/Login'
import Logout from './components/Logout'
import Register from './components/Register'
import Application from './components/Application'

import './App.css';
import './fonts/stylesheet.css'


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    loggedIn() ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={withTracker(Home)}/>
          <Route exact path="/about/" component={withTracker(Page)} />
          <Route exact path="/register/" component={withTracker(Register)} />
          <Route exact path="/login/" component={withTracker(Login)} />
          <Route exact path="/connect/" component={withTracker(Contact)} />
          <Route exact path="/partner/" component={withTracker(Partner)} />
          <PrivateRoute exact path="/logout/" component={withTracker(Logout)} />
          <PrivateRoute exact path="/dashboard/apply/" component={withTracker(Application)} />
          <Route exact path="/:category" component={withTracker(Requirement)}/>
          <Route component={NotFound}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
