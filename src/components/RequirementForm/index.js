import React, { Component } from 'react';
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import Dropzone from 'react-dropzone'
import { Link } from 'react-router-dom'

import Navbar from '../Navbar'
import '../ContactForm/ContactForm.css'
import Footer from '../Footer'

import {createDocument, requirement} from '../../API'

class ScrollToTopOnMount extends Component {
  componentDidMount(prevProps) {
    window.scrollTo(0, 0)
  }

  render() {
    return null
  }
}

export default class RequirementForm extends Component {
	state = {
		categories: [],
		files: [],
		loading: true,
		// fields
	  first_name: '',
	  last_name: '',
	  email: '',
	  phone: '',
	  industry: '',
	  description: '',
	  skills: '',
	  category: [],
	  documents: [],
	}

	handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    if (target.type === 'checkbox') {
    	var category = new Set(this.state.category)
    	if (target.checked) {
    		// eslint-disable-next-line
    		category.add(parseInt(value))
    	} else {
    		// eslint-disable-next-line
    		category.delete(parseInt(value))
    	}
    	
    	this.setState({
	      [name]: [...category]
	    });
    } else {
	    this.setState({
	      [name]: value
	    });
    }
	}

  onDrop = files => {this.setState({files})}
  
  componentDidMount = () => {
    if (this.props.category) {
	    this.setState({
	    	category: this.state.category.concat([this.props.category.id]),
	    	loading: false
	    })
    }
  }

  uploadDocuments = () => {
		const requests = []
		this.state.files.map(file => {
	  	var formData = new FormData()
	  	formData.append("name", file.name)
	  	formData.append("file", file)

			return requests.push(createDocument(formData))
		})
		
		if (!isEmpty(requests)) {
			return axios.all(requests)
			.then(results => {
				var documents = this.state.documents
				results.map(res => documents.push(res.data.id))
				this.setState({documents})
			})
			.catch(error => console.log(error));
		} else {

		}
	}

	handleSubmit = e => {
		this.setState({
			submitting: true,
		})

		if (!isEmpty(this.state.files)) {
			this.uploadDocuments()
				.then(() => {
					var formData = Object.assign({}, this.state);
					delete formData.categories
					return requirement(formData)
				})
				.then(response => {
					this.setState({
						submitted: true,
					})
					// todo this.resetForm()
				})
				.catch(error => {
					this.setState({
						errors: error.response.data,
						submitting: false,
					})
				})
		} else {
			this.setState({
				submitting: false,
				validDocument: true,
			})
		}

		e.preventDefault()
	}

	renderForm = (categories, validDocument) => {
		return (

			<section>
			 <ScrollToTopOnMount/>
				<div className="container container-width">
					<div className="row">
						<h1>Brief us</h1>
						<form onSubmit={this.handleSubmit} id="contactForm">
							<div className="row">
								<div className="six columns">
									<label>First Name</label>
									<input className="u-full-width" type="text" value={this.state.first_name} name="first_name" onChange={this.handleChange} required/>
								</div>
								<div className="six columns">
									<label>Last Name</label>
									<input className="u-full-width" type="text" value={this.state.last_name} name="last_name" onChange={this.handleChange} required/>
								</div>
							</div>

							<div className="row">
								<div className="six columns">
									<label>Email</label>
									<input className="u-full-width"  type="email" value={this.state.email} name="email" onChange={this.handleChange} required/>
								</div>
								<div className="six columns">
									<label>Phone</label>
									<input className="u-full-width"  type="text" value={this.state.phone} name="phone" onChange={this.handleChange} required/>
								</div>
							</div>

							<div className="row">
								<div className="twelve columns">
									<label>What are you looking to get done?</label>
									<ul>
										{categories ? 
											categories.map(category => {
												return (
													<label key={category.id}>
												    <input 
												    	type="checkbox" 
												    	name="category"
												    	value={category.id}
												    	defaultChecked={this.state.category.includes(category.id)}
												    	onChange={this.handleChange}
												    	/>
												    {" " + category.name}
												  </label>
												)
											})
											: null}
									</ul>
								</div>
							</div>

							<div className="row">
								<div className="twelve columns">
									<label>Your Industry (e.g. E-commerce, Food, Travel)</label>
									<input className="u-full-width"  type="text" value={this.state.industry} name="industry" onChange={this.handleChange} required/>
								</div>
							</div>

							<div className="row">
								<div className="twelve columns">
									<label>
										What is it all about? <br />
										(Tell us what your project is all about and Specify your requirements of the work to be done)
									</label>
									<textarea className="u-full-width"  type="text" value={this.state.description} name="description" onChange={this.handleChange} required/>
								</div>
							</div>

							<div className="row">
								<div className="twelve columns">
									<label>
									Your Preferred Skills for this project <br /> 
									(Leave this empty if you want our recommendation)
									</label>
									<input className="u-full-width"  type="text" value={this.state.skills} name="skills" onChange={this.handleChange}/>
								</div>
							</div>
							<div className="row">
								<div className="six columns">
									<label>
									Attach Document
									</label>
				          <Dropzone onDrop={this.onDrop} required>
				            <p style={{margin:'1rem'}}>Try dropping some files here, or click to select files to upload.</p>
				          </Dropzone>
								</div>
								<div className="six columns">
				          <ul>
				            {
				              this.state.files.map(f => <li key={f.name}><b>{f.name}</b> - {f.size} bytes</li>)
				            }
				          </ul>
									{validDocument ? 
										<div>
										<strong style={{color: 'red'}}>Please upload documents</strong>
										</div>
									: null}
								</div>
							</div>
							<br />
							<input className="button-primary" type="submit" value="Submit"/>
						</form>
					</div>
				</div>
			</section>
		)
	}

	renderSuccess = () => {
		return (
			<div>
				<Navbar />
				<section>
					<div className="container center container-width centerFlex">
						<div className="row">
						  <h4>Thank you for giving us the requirement, <br /> 
						  We will email you the Budget and Timeline for this project in the next 24 hours!
						  </h4>
						  <strong>Go to <Link to="/">HOME</Link></strong> 
						</div>
					</div>
				</section>
			</div>
		)
	}

	rednerSpinner = () => {
		return (
			<div>
				<Navbar />
				<section>
					<div className="container center container-width centerFlex">
						<div className="row">
							<div className="spinner"></div>
						</div>
					</div>
				</section>
			</div>
		)
	}

	render() {
		const {submitted, loading, validDocument} = this.state
		const {categories} = this.props

		if (submitted) {
			return this.renderSuccess()
		}

		if (loading) {
			return <div></div>
		}

		return (
			<div>
			<Navbar navs={localStorage.token ? 
        [
				{url: '/dashboard/apply/', name: 'My Profile'},
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/connect/', name: 'Connect'},
				{url: '/logout/', name: 'Logout'},
				] : [
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/partner/', name: 'Work on Projects'},
        {url: '/connect/', name: 'Connect'},]
      }/>
				{this.state.submitting ? this.rednerSpinner():this.renderForm(categories, validDocument)}
				<Footer />
			</div>
			);
	}
}
