import React, { Component } from 'react'

import {vouch, vouchDetail, freelancerDetail} from '../../API'

export default class Vouch extends Component {
	state = {
		vouch1: null,
		name1: '',
    email1: '',
    company1: '',
    position1: '',
    vouch2: null,
		name2: '',
    email2: '',
    company2: '',
    position2: '',
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleSubmit = e => {
		e.preventDefault()

		this.setState({
			submitting: true,
		})

		vouch(this.state)
			.then(response => {
				this.setState({
					submitted: true,
					submitting: false,
				})
				setTimeout(() => this.setState({submitted: false}), 1500)
				this.componentDidMount()		
			})
			.catch(error => {
				this.setState({
					errors: error.response.data,
					submitting: false,
				})
			})
	}

  componentDidMount = () => {
    freelancerDetail()
    	.then(res => {
    		res.data.vouches.forEach((vouch, vouchIndex) => {
    			return vouchDetail(vouch)
    				.then(res => {
    					let index = vouchIndex + 1
    					this.setState({
    						['vouch' + index]: res.data.id,
    						['name' + index]: res.data.name,
    						['email' + index]: res.data.email,
    						['company' + index]: res.data.company,
    						['position' + index]: res.data.position,
    					})
    				})
    		})
    	})
  }

	render() {
/*		console.log('vouch', this.state)*/
    return (
    	<div>
    		<br />
    		<br />
 				<h4>Who can vouch for you?</h4>
				<form onSubmit={this.handleSubmit} id="contactForm">
				<h5>Vouch 1</h5>
				<div className="row">
					<div className="six columns">
						<label>Name</label>
						<input className="u-full-width" type="text" value={this.state.name1} name="name1" onChange={this.handleChange} required/>
					</div>
					<div className="six columns">
						<label>Email</label>
						<input className="u-full-width" type="email" value={this.state.email1} name="email1" onChange={this.handleChange} required/>
					</div>
				</div>

				<div className="row">
					<div className="six columns">
						<label>Company</label>
						<input className="u-full-width"  type="text" value={this.state.company1} name="company1" onChange={this.handleChange} required/>
					</div>
					<div className="six columns">
						<label>Position</label>
						<input className="u-full-width"  type="text" value={this.state.position1} name="position1" onChange={this.handleChange} required/>
					</div>
				</div>

				<h5>Vouch 2</h5>
				<div className="row">
					<div className="six columns">
						<label>Name</label>
						<input className="u-full-width" type="text" value={this.state.name2} name="name2" onChange={this.handleChange}/>
					</div>
					<div className="six columns">
						<label>Email</label>
						<input className="u-full-width" type="email" value={this.state.email2} name="email2" onChange={this.handleChange}/>
					</div>
				</div>

				<div className="row">
					<div className="six columns">
						<label>Company</label>
						<input className="u-full-width"  type="text" value={this.state.company2} name="company2" onChange={this.handleChange}/>
					</div>
					<div className="six columns">
						<label>Position</label>
						<input className="u-full-width"  type="text" value={this.state.position2} name="position2" onChange={this.handleChange}/>
					</div>
				</div>
					<br />
					<input className="button-primary u-pull-right" type="submit" value="Save"/>
						{this.state.submitting ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saving...</span> : null}
						{this.state.submitted ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saved.</span>: null}
				</form>
      </div>
    )
	}
}


