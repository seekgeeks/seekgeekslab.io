import React, { Component } from 'react'

import { profileStrength } from '../../API'

export default class ProfilePicture extends Component {
	state = {
		profile_strength: null
	}

  apply = () => {
    profileStrength()
    	.then(res => this.setState({ profile_strength: res.data }))
  }

	render() {
		const { profile_strength } = this.state
    return (
    	<div>
	    	<h4>Apply</h4>
	    	<p>
	    		After filling up all the sections of your profile,
	    		please click on apply to send your application to us.
	    	</p>
	    	<input type='button' onClick={this.apply} value='Apply'/>
	    	{profile_strength &&
	    		<ul>
			    	<li style={{color: 'red', listStyle: 'none'}}>{'error' in profile_strength.basic_info && profile_strength.basic_info.error}</li>
		      	<li style={{color: 'red', listStyle: 'none'}}>{'error' in profile_strength.web_presence && profile_strength.web_presence.error}</li>
		      	<li style={{color: 'red', listStyle: 'none'}}>{'error' in profile_strength.freelancer && profile_strength.freelancer.error}</li>
		      	<li style={{color: 'red', listStyle: 'none'}}>{'error' in profile_strength.vouch && profile_strength.vouch.error}</li>
		      	<li style={{color: 'green', listStyle: 'none'}}>{'submitted' in profile_strength && profile_strength.submitted_msg}</li>
		      </ul>
	    	}

      </div>
    )
	}
}
