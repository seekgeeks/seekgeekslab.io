import React, { Component } from 'react'

import {profile, profileDetail} from '../../API'

export default class BasicInfo extends Component {
	state = {
		first_name: '',
		last_name: '',
		location: '',
		bio: '',
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleSubmit = e => {
		e.preventDefault()

		this.setState({
			submitting: true,
			error:[],
		})

		profile(this.state)
			.then(response => {
				this.setState({
					submitted: true,
					submitting: false,
					error:[],
				})
				setTimeout(() => this.setState({submitted: false}), 1500)				
			})
			.catch(error => {
				this.setState({
					errors: error.response.data,
					submitting: false,
				})
			})
	}

	componentDidMount = () => {
		profileDetail()
			.then(res => this.setState({
				first_name: res.data.first_name,
				last_name: res.data.last_name,
				location: res.data.location,
				bio: res.data.bio,
				interests: res.data.interests,
			}))
	}


	render() {
		return (
			<div>
				<h4>Basic Info</h4>
				<form onSubmit={this.handleSubmit} id="contactForm">
					<div className="row">
						<div className="six columns">
							<label>First Name</label>
							<input className="u-full-width" type="text" value={this.state.first_name} name="first_name" onChange={this.handleChange} required/>
						</div>

						<div className="six columns">
							<label>Last Name</label>
							<input className="u-full-width" type="text" value={this.state.last_name} name="last_name" onChange={this.handleChange} required/>
						</div>
					</div>

					<div className="row">
						<div className="twelve columns">
							<label>Where are you based (city and country name)?</label>
							<input className="u-full-width"  type="text" value={this.state.location} name="location" onChange={this.handleChange} required/>
						</div>
					</div>

					<div className="row">
						<div className="twelve columns">
							<label>About Yourself</label>
							<textarea
								className="u-full-width" 
								value={this.state.bio}
								name="bio"
								onChange={this.handleChange}
								required/>
						</div>
					</div>
					<br />
					<input className="button-primary u-pull-right" type="submit" value="Save"/>
					{this.state.submitting ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saving...</span> : null}
					{this.state.submitted ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saved.</span>: null}
				</form>
				<br />
			</div>
		) 
	}	
}