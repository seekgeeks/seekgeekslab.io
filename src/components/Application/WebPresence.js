import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';

import {profile, profileDetail} from '../../API'

export default class WebPresence extends Component {
	state = {
		website: '',
	  facebook: '',
	  twitter: '',
	  linkedin: '',
	  skype: '',
	  other_website: '',
	  atLeastOne: false,
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	handleSubmit = e => {
		e.preventDefault()
		const {website, facebook, twitter, linkedin, skype, other_website} = this.state
		
		if (!!website || !!facebook || !!twitter || !!linkedin || !!skype || !!other_website) {
		this.setState({
			submitting: true,
			errors: [],
		})

			profile(this.state)
				.then(response => {
					this.setState({
						atLeastOne: false,
						submitted: true,
						submitting: false,
					})
					setTimeout(() => this.setState({submitted: false}), 1500)
				})
				.catch(error => {
					this.setState({
						atLeastOne: false,
						errors: error.response.data,
						submitting: false,
					})
				})
		} else {
			this.setState({
				atLeastOne: true,
			})
		}
	}

	componentDidMount = () => {
		profileDetail()
			.then(res => {
				const {website, facebook, twitter, linkedin, skype, other_website} = res.data
				if ((!!website || !!facebook || !!twitter || !!linkedin || !!skype || !!other_website)) {
					this.setState({
						website,
						facebook,
						twitter,
						linkedin,
						skype,
						other_website,
						atLeastOne: false,
					})
				} else {
					this.setState({
						atLeastOne: true,
					})
				}
			})
	}

	renderError = (field) => {
		if (this.state.errors && field in this.state.errors) {
			return (
				<ul>
				{this.state.errors[field].map(element => <li style={{color: 'red'}}>{element}</li>)}
				</ul>
			)
		}
	}

	render() {

		return (
			<div>
				<h4>Web Presence</h4>
					<p style={{color: 'grey'}}>Copy and paste the links of your website & social profiles. Make sure to use 'http://' or 'https://' before the link</p>
					<form onSubmit={this.handleSubmit} id="contactForm">

						<div className="row">
							<div className="twelve columns">
								<label>Website</label>
								<input 
									className="u-full-width"
									type="text"
									value={this.state.website}
									name="website"
									onChange={this.handleChange}
									/>
									{this.renderError('website')}
							</div>
						</div>

						<div className="row">
							<div className="twelve columns">
								<label>Facebook</label>
								<input 
									className="u-full-width"
									type="text"
									value={this.state.facebook}
									name="facebook"
									onChange={this.handleChange}
									/>
									{this.renderError('facebook')}
							</div>
						</div>

						<div className="row">
							<div className="twelve columns">
								<label>Twitter</label>
								<input 
									className="u-full-width"
									type="text"
									value={this.state.twitter}
									name="twitter"
									onChange={this.handleChange}
									/>
									{this.renderError('twitter')}
							</div>
						</div>

						<div className="row">
							<div className="twelve columns">
								<label>LinkedIn</label>
								<input 
									className="u-full-width"
									type="text"
									value={this.state.linkedin}
									name="linkedin"
									onChange={this.handleChange}
									/>
									{this.renderError('linkedin')}
							</div>
						</div>

						<div className="row">
							<div className="twelve columns">
								<label>Skype</label>
								<input 
									className="u-full-width"
									type="text"
									value={this.state.skype}
									name="skype"
									onChange={this.handleChange}
									/>
									{this.renderError('skype')}
							</div>
						</div>

						<div className="row">
							<div className="twelve columns">
								<label>Other Website</label>
								<input 
									className="u-full-width"
									type="text"
									value={this.state.other_website}
									name="other_website"
									onChange={this.handleChange}
									/>
									{this.renderError('other_website')}
							</div>
						</div>

						<br />
						<input className="button-primary u-pull-right" type="submit" value="Save"/>
						{this.state.submitting ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saving...</span> : null}
						{this.state.submitted ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saved.</span>: null}
						{!isEmpty(this.state.errors) ? <span className="u-pull-right" style={{color: 'red', padding: '1rem'}}>Please check the errors.</span>: null}
						{this.state.atLeastOne ? <span className="u-pull-right" style={{color: 'red', padding: '1rem'}}>At Least one website required.</span>: null}
					</form>
					<br />
			</div>
		) 
	}	
}