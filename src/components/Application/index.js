import React, { Component } from 'react'

import Navbar from '../Navbar'
import BasicInfo from './BasicInfo'
import WebPresence from './WebPresence'
import IndividualOrCompany from './IndividualOrCompany'
import Vouch from './Vouch'
import Apply from './Apply'

import Footer from '../Footer'

export default class Application extends Component {
	render() {
		return (
			<div>
			<Navbar navs={[
				{url: '/connect', name: 'Connect'},
				{url: '/logout', name: 'Logout'},
				]}/>
			<section>
				<div className="container">
				  <div className="row">
				  	<br />
				  	<h2 className="center">Profile</h2>
				    <div className="seven columns">
				    	<BasicInfo />
				    	<WebPresence />
				    	<IndividualOrCompany />
				    	<Vouch />
				    	<br />
				    	<p>Click on on apply once you have filled all the details.</p>
				    </div>
				    <div className="four columns">
				    	<Apply />
				    </div>
				  </div>
				</div>
			</section>
			<Footer />
			</div>
		)
	}
}
