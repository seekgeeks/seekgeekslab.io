import React, { Component } from 'react';
import axios from 'axios';
import Dropzone from 'react-dropzone'
import isEmpty from 'lodash/isEmpty';

import {getCategories, freelancer, freelancerDetail, createDocument, deleteDocument} from '../../API'

export default class IndividualOrCompany extends Component {
	state = {
		categories: [],
		files: [],
		documents: [],
		freelancer_type: 1,
		company: '',
		service_offered: [],
		phone: '',
		hours: '',
		skills: '',
		portfolio: '',
		about_me: '',
		other_info: '',
	}

	handleOptionChange = event => {
    this.setState({
    	// eslint-disable-next-line
      freelancer_type: parseInt(event.target.value)
    });
	}

	handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    if (target.type === 'checkbox') {
    	var service_offered = new Set(this.state.service_offered)
    	if (target.checked) {
    		// eslint-disable-next-line
    		service_offered.add(parseInt(value))
    	} else {
    		// eslint-disable-next-line
    		service_offered.delete(parseInt(value))
    	}
    	
    	this.setState({
	      [name]: [...service_offered]
	    });
    } else {
	    this.setState({
	      [name]: value
	    });
    }
	}

  onDrop = files => {this.setState({files})}

  uploadDocuments = () => {

		if (isEmpty(this.state.files)) {
			return Promise.resolve()
		}

		const requests = []

		this.state.files.map(file => {
	  	var formData = new FormData()
	  	formData.append("name", file.name)
	  	formData.append("file", file)

			return requests.push(createDocument(formData))
		})
		
		if (!isEmpty(requests)) {
			return axios.all(requests)
			.then(results => {
				var documents = this.state.documents
				results.map(res => documents.push(res.data))
				this.setState({documents})
			})
			.catch(error => console.log(error));
		}
	}

	handleSubmit = e => {
		e.preventDefault()

		this.setState({
			submitting: true,
			errors: [],
		})

		this.uploadDocuments()
			.then(() => {
				const payload = this.state
				payload.documents = payload.documents.map(doc => doc.id)
				return freelancer(payload)
			})
			.then(response => {
				this.setState({
					submitted: true,
					submitting: false,
					files: [],
					errors: [],
				})
				setTimeout(() => this.setState({submitted: false}), 2000)
				this.componentDidMount()
			})
			.catch(error => {
				this.setState({
					errors: error.response.data,
					submitting: false,
				})
			})
	}

	deleteDoc = (id) => {
		if (id) {
			deleteDocument(id)
				.then(res => {
			    this.componentDidMount()
				})
		}
	} 

  componentDidMount = () => {
    getCategories()
    	.then(res => this.setState({ categories: res.data }))

    freelancerDetail()
    	.then(res => {
				this.setState({
					freelancer_type: res.data.freelancer_type,
			 		company: res.data.company,
			 		documents: res.data.documents,
					phone: res.data.phone,
					hours: res.data.hours,
					skills: res.data.skills,
					portfolio: res.data.portfolio,
					about_me: res.data.about_me,
					other_info: res.data.other_info,
					service_offered: res.data.service_offered,
				})
    	})
  }

	renderError = (field) => {
		if (this.state.errors && field in this.state.errors && field === 'documents') {
			return (
				<ul>
					<li style={{color: 'red'}}>Please upload at least one document.</li>
				</ul>
			)
		}
		if (this.state.errors && field in this.state.errors) {
			return (
				<ul>
				{this.state.errors[field].map(element => <li style={{color: 'red'}}>{element}</li>)}
				</ul>
			)
		}
	}

	render() {
		const {categories} = this.state
		return (
			<div>
			<h4>Individual or Company</h4>
			<form onSubmit={this.handleSubmit} id="contactForm">

				<div className="row">
					<div className="twelve columns">
					  <input 
					  	type="radio"
					  	value={1}
							checked={this.state.freelancer_type === 1}
							onChange={this.handleOptionChange}
					  	/> Individual &nbsp; &nbsp;
					  <input 
					  	type="radio"
					  	value={2}
							checked={this.state.freelancer_type === 2}
							onChange={this.handleOptionChange}
					  	/> Company<br />
					</div>
				</div>

				{this.state.freelancer_type === 2 ? 
					<div className="row">
						<div className="twelve columns">
							<label>Company Name</label>
							<input 
								className="u-full-width"
								type="text"
								value={this.state.company}
								name="company"
								onChange={this.handleChange}
								/>
						</div>
					</div>
				:null}
				{this.renderError('freelancer_type')}
				<div className="row">
					<div className="twelve columns">
						<label>Please share your contact number (It’s safe with us, promise)</label>
						<input 
							className="u-full-width"
							type="text"
							value={this.state.phone}
							name="phone"
							onChange={this.handleChange}
							required
							/>
							{this.renderError('phone')}
					</div>
				</div>

				<div className="row">
					<div className="twelve columns">
						<label>What are the services that you provide?</label>
						<ul>
							{categories ? 
								categories.map(category => {
									return (
										<label key={category.id}>
									    <input 
									    	type="checkbox" 
									    	name="service_offered"
									    	value={category.id}
									    	checked={this.state.service_offered.includes(category.id)}
									    	onChange={this.handleChange}
									    	/>
									    &nbsp; {category.name}
									    {this.renderError('category')}
									  </label>
									)
								})
								: null}
						</ul>
					</div>
				</div>

				<div className="row">
					<div className="twelve columns">
						<label>Hours per day/ week as instruction text</label>
						<input 
							className="u-full-width"
							type="text"
							value={this.state.hours}
							name="hours"
							onChange={this.handleChange}
							/>
							{this.renderError('huours')}
					</div>
				</div>


				<div className="row">
					<div className="twelve columns">
						<label>What are your skills? 
						Enter comma ( , ) separated values like:
						Django, CakePHP, Ruby, Node.js, Angular.js, Meteor.js,
						MongoDB, MySQL, Heroku, AWS, etc.</label>
						<input 
							className="u-full-width"
							type="text"
							value={this.state.skills}
							name="skills"
							onChange={this.handleChange}
							required
							/>
							{this.renderError('skills')}
					</div>
				</div>


				<div className="row">
					<div className="twelve columns">
						<label>Please share your work portfolio with us? Links?</label>
						<textarea
							className="u-full-width" 
							type="text"
							name="portfolio"
							value={this.state.portfolio}
							onChange={this.handleChange}
							required/>
							{this.renderError('portfolio')}
					</div>
				</div>

				<div className="row">
					<div className="six columns">
						<br />
						<label>
						Please upload your portfolio, resume. Or just drag & drop.
						</label>
						{this.renderError('documents')}
	          <Dropzone onDrop={this.onDrop} required>
	            <p style={{margin:'1rem'}}>
	            Drag & Drop the file here Or click to select the file to upload.
	            </p>
	          </Dropzone>
					</div>
					<div className="six columns">
						<br />
						{!isEmpty(this.state.files) ?
							<div>
								<label>Dropped files</label>
			          <ul>
			            {
			              this.state.files.map((f,i) => <li key={i}>{f.name} - {f.size} bytes</li>)
			            }
			          </ul>
		          </div>
						: null}
	          {!isEmpty(this.state.documents) ? 
	          	<div>
			          <label>Uploaded files</label>
			          <ul>
			            {
			              this.state.documents.map(f =>
			              	<li key={f.id + f.file}>
			              		{f.name} - <a className="button" href={f.file} download>Download</a>
			              		&nbsp;
			              		&nbsp;
			              		<input type="button" style={{color: 'red'}} onClick={() => this.deleteDoc(f.id)} value='Remove' />
			              	</li>)
			            }
			          </ul>
	          	</div>
	          : null}
					</div>
				</div>
				<br />
				<br />
				<div className="row">
					<div className="twelve columns">
						<label>
						Tell us about yourself and the most impressive thing 
						about your work or something that you have accomplished? 
						(Don’t be modest)</label>
						<textarea
							className="u-full-width" 
							type="text"
							value={this.state.about_me}
							name="about_me"
							onChange={this.handleChange}
							required/>
						{this.renderError('about_me')}
					</div>
				</div>


				<div className="row">
					<div className="twelve columns">
						<label>Anything else we should know about you?</label>
						<textarea
							className="u-full-width" 
							type="text"
							value={this.state.other_info}
							name="other_info"
							onChange={this.handleChange}
							required/>
							{this.renderError('other_info')}
					</div>
				</div>

				<br />
				<input className="button-primary u-pull-right" type="submit" value="Save"/>
				{this.state.submitting && !this.state.submitted ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saving...</span> : null}
				{this.state.submitted && !this.state.submitting ? <span className="u-pull-right" style={{color: 'green', padding: '1rem'}}>Saved.</span>: null}
				{!isEmpty(this.state.errors) ? <span className="u-pull-right" style={{color: 'red', padding: '1rem'}}>Please check the errors.</span>: null}
			</form>
			</div>
		) 
	}	
}