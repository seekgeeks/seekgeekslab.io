import React, { Component } from 'react';
import {Link} from  'react-router-dom'

import Section from '../Section'

export default class Footer extends Component {

  footer = {
    id: 'footer',
    center: true,
    Content: () => {
      return <div>
        <Link className="footer-item" to='/about/'>
          About
        </Link>
        <a target="_blank" rel="noopener noreferrer" className="footer-item" href="https://www.facebook.com/seekgeeks/">Facebook</a>
        <a target="_blank" rel="noopener noreferrer" className="footer-item" href="https://twitter.com/seekgeeks">Twitter</a>
        <p>
          <small>© 2017 SeekGeeks. All rights reserved.</small>
        </p>
      </div>
    }
  }

	render() {
		return <Section section={this.footer}/>
	}
}
