import React, { Component } from 'react';

import Navbar from '../Navbar'
import Section from '../Section'
import ContactForm from '../ContactForm'
import Footer from '../Footer'

export default class Contact extends Component {

  connect = {
    id: 'connect',
    title: 'Drop us a message!',
    subTitle: 'Have any Feedback, Question or Query? Reach out to us today. Get rid of the slow processes and unnecessary frictions.',
    Content: () => {
      return <ContactForm />
    }
  }

	render() {
		return (
			<div>
			<Navbar navs={localStorage.token ? 
        [
				{url: '/dashboard/apply/', name: 'My Profile'},
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/connect/', name: 'Connect'},
				{url: '/logout/', name: 'Logout'},
				] : [
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/partner/', name: 'Work on Projects'},
        ]
      }/>
 			<br />
      <br />
			<Section section={this.connect}/>
			<Footer />
			</div>
		);
	}
}
