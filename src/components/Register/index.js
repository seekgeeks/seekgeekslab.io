import React, { Component } from 'react';
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import { Link, Redirect } from 'react-router-dom'

import { login, registerAuthURL } from '../../Auth'
import Navbar from '../Navbar'
import Footer from '../Footer'

export default class Register extends Component {
	state = {
		username: '',
		email:'',
		password: '',
		confirm_password: '',
		first_name: '',
		last_name: '',
		redirect: false,
	}

	handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
	}

	handleSubmit = e => {
		this.setState({
			submitting: true,
		})

		const formData = this.state

		axios.post(registerAuthURL, formData)
			.then(response => {
				this.setState({
					submitted: true,
				})
		    login(this.state.username, this.state.password)
		    	.then(() => this.setState({redirect:true}))
					})
			.catch(error => {
				this.setState({
					errors: error.response.data,
					submitting: false,
				})
			})

		e.preventDefault()
	}

	renderErrors = () => {
		const {errors} = this.state
		return !isEmpty(errors) 
			&& 'non_field_errors' in errors 
			&& <span style={{color: 'red', margin: '1rem'}}>
				{errors.non_field_errors[0]}
				<br />
				</span>
	}

	renderError = (field) => {
		if (this.state.errors && field in this.state.errors) {
			return (
				<ul>
				{this.state.errors[field].map(element => <li style={{color: 'red'}}>{element}</li>)}
				</ul>
			)
		}
	}

	render() {

    if (this.state.redirect || localStorage.token) {
      return (
        <Redirect to={'/dashboard/apply'}/>
      )
    }
		return (
			<div>
			<Navbar navs={localStorage.token ? 
        [
				{url: '/dashboard/apply/', name: 'My Profile'},
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/connect/', name: 'Connect'},
				{url: '/logout/', name: 'Logout'},
				] : [
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/partner/', name: 'Work on Projects'},
        {url: '/connect/', name: 'Connect'}]
      }/>
			<br />
			<br />
			<br />
			<section>
				<div className="container center">
					<div className="row">
						<h1>Create account</h1>
						<form onSubmit={this.handleSubmit} id="contactForm">
							<div className="row">
								<div className="six columns" style={Styles}>
									<label>Username</label>
									<input className="u-full-width" type="text" value={this.state.username} name="username" onChange={this.handleChange} required/>
									{this.renderError('username')}
									<label>Email</label>
									<input className="u-full-width"  type="email" value={this.state.email} name="email" onChange={this.handleChange} required/>
									{this.renderError('email')}
									<label>Password</label>
									<input className="u-full-width" type="password" value={this.state.password} name="password" onChange={this.handleChange} required/>
									{this.renderError('password')}
									<label>Confirm Password</label>
									<input className="u-full-width" type="password" value={this.state.confirm_password} name="confirm_password" onChange={this.handleChange} required/>							
									{this.renderError('confirm_password')}
								</div>
							</div>
							{this.renderErrors()}
							<br />
							<input className="button-primary" type="submit" value="Create account"/>
						</form>
						<p>Already have an account? <Link to="/login/">Log In</Link></p>
					</div>
				</div>
			</section>
			<Footer />
			</div>
		);
	}
}

const Styles = {
	float: 'none',
	margin: '0 auto'
}
