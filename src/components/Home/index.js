import React, { Component } from 'react';
import chunk from 'lodash/chunk';
import {Link} from  'react-router-dom'
import CountTo from 'react-count-to';


import {getCategories} from '../../API'
import Navbar from '../Navbar'
import Section from '../Section'
import Footer from '../Footer'

export default class Home extends Component {
  state = {
    categories:null
  }

  navs = () => {
    let navs_to_return = [
      { url: '#howitworks', name: 'How it Works' },
      { url: '#services', name: 'Services' },
      { url: '/connect/', name: 'Connect' },
      { url: '/partner/', name: 'Work on Projects',}
    ]
    if (localStorage.token) {
      navs_to_return.push({url: '/dashboard/apply/', name:'Dashboard', dashboard: true})
    }
    return navs_to_return
  } 

  sections = () => ({
      howitworks: {
        id: 'howitworks',
        title: 'How it works',
        center: true,
        subTitle: 'We curate talent that believes in delivering seamless & exceptional experiences to create an impact.',
        icons: [
          {
            id: 1,
            src: 'https://res.cloudinary.com/seekgeeks/image/upload/q_auto/v1511589713/if_document-04_1622832_x6eagk.png',
            alt: 'Send us your specific work requirements'
          },{
            id: 2,
            src: 'https://res.cloudinary.com/seekgeeks/image/upload/q_auto/v1511589713/if_document-10_1622826_fkymcu.png',
            alt: 'We connect you with the most suitable talent'
          },{
            id: 3,
            src: 'https://res.cloudinary.com/seekgeeks/image/upload/q_auto/v1511589713/if_document-03_1622833_smqbqw.png',
            alt: 'You approve the process. We get the work started'
          },{
            id: 4,
            src: 'https://res.cloudinary.com/seekgeeks/image/upload/q_auto/v1511589713/if_documents-07_1622836_spfu7z.png',
            alt: 'We get your work done & delivered'
          }
        ],
      },
      services: {
        id: 'services',
        title: 'You can trust us with',
        center: true,
        subTitle: 'Click on the category of your work requirement to get started',
        Content: () => {
          const categories = chunk(this.state.categories, this.state.categories && this.state.categories.length/3)
          return  (this.state.categories && categories ?
            <div> {categories.map((array, index) => {
            return <div className="one-third column" key={index}>
              {array.map(item => {
              return (
                <div className="services-box" key={item.id}>
                  <Link className="button button-primary service-item" to={item.slug + '/'}>{item.name}</Link>
                </div>)
            })}</div>;
          })} </div>: <div> Loading...</div>)
        }
      }
    })

  updateHeight() {
    var winHeight = {height: window.innerHeight};
    this.setState({ winHeight });
  }

  componentDidMount() {
    this.updateHeight()
    getCategories()
      .then(res => {
        this.setState({ categories: res.data })
      })
    window.addEventListener("resize", this.updateHeight.bind(this));
  }

  render() {
    const sections = this.sections()
    return (
      <div>
        <Navbar navs={this.navs()}/>
        <section id="hero" className="hero u-full-width" style={this.state.winHeight}>
          <div className="hero-image"></div>
          <div className="container centered">
            <div className="twelve columns">
              <h1 className="title">Outsource your tech, design, marketing and more works to the most credible geeks and agencies</h1>
              <a className="button button-primary button-font" href="#services">Start a Project</a>
            </div>
          </div>
        </section>
        <Section section={sections.howitworks} key='howitworks'/>
        <Section section={sections.services} key='services'/>
        <section id="hero" className="hero u-full-width">
          <div className="achievement-image"></div>
          <div className="container centered">
            <div className="row">
              <div className="three columns">
                <h2 className="title" style={{fontWeight: 'bold'}}>
                <CountTo to={40} speed={2000} />+</h2>
                <h4>Freelancers</h4>
              </div>
              <div className="three columns">
                <h2 className="title" style={{fontWeight: 'bold'}}>
                <CountTo to={10} speed={3000} />+</h2>
                <h4>Agencies</h4>
              </div>
              <div className="three columns">
                <h2 className="title" style={{fontWeight: 'bold'}}>
                <CountTo to={200} speed={4000} />+</h2>
                <h4>Hours in works</h4>
              </div>
              <div className="three columns">
                <h2 className="title" style={{fontWeight: 'bold'}}><CountTo to={2500} speed={5500} />+</h2>
                <h4>Cups of coffee</h4>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    )
  }
}
