import React, {Component} from 'react';
import {Redirect} from 'react-router-dom'

import {logout} from '../../Auth'

export default class Logout extends Component {
  state = {
    redirect: false
  }

  componentWillMount = () => {
    logout()
    this.setState({redirect: true})
  }

  render() {
    const redirect = this.state
    return redirect ? <Redirect to={{ pathname: '/login/' }}/> : null
  }
}