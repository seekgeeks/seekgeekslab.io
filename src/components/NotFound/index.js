import React, { Component } from 'react';

import Navbar from '../Navbar'
import Section from '../Section'
import Footer from '../Footer'

import {Link} from 'react-router-dom'

const notFound = {
  id: 'notFound',
  title: '404 Not Found :(',
 }

export default class NotFound extends Component {
	
  render() {	
    return (
      <div>
        <Navbar navs={[]}/>
        <br />
        <br />
        <br />
        <br />
        <br />
        <Section section={notFound}>
          <p>
            The page you are trying open is not found, Please go to <Link to="/">Home</Link>
          </p>
        </Section>
        <Footer />
      </div>
    );
  }
}