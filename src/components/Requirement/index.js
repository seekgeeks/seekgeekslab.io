import React, { Component } from 'react';

import NotFound from '../NotFound'
import RequirementForm from '../RequirementForm'
import {getCategories} from '../../API'

export default class Requirement extends Component {
  state = {
    categories: null,
    loading: true,
  }

  componentDidMount() {
    getCategories()
      .then(res => {
        this.setState({ categories: res.data, loading:false })
      })
  }

  render() {
  	const { categories, loading } = this.state
  	const { match } = this.props
    
    if (loading) {
      return <div></div>
    }

		if (categories && categories.find(category => category.slug === match.params.category)) {
	    return (
	    	<RequirementForm
	        category={categories.find(category => category.slug === match.params.category)}
          categories={categories}
	        />
	     	)
	  } else {
	    return <NotFound />
	  }
	}
}
