import React from 'react';

import Icon from '../Icon'

const Section = ({section, children}) => {
  const {id, title, center, subTitle, centerSubTitle, icons, actionable, Content} = section
	return (
    <section id={id} className={id + ' u-full-width ' + (center && 'center')}>
      <div className="container">
        <div className="row">
          <div className="twelve columns">
            {title && <h2 className="separator">{title}</h2>}
            {subTitle && <p className={centerSubTitle ? 'center description' : 'description'}>{subTitle}</p>}
            {icons && 
              <div className="row icons">
                {icons.map(item => <Icon icon={item} key={item.id}/>)}
              </div>
            }
            {Content && 
              <div className="row">
                <Content />
              </div>
            }
            {children && <div className="row">
              {children}
              </div>}
            {actionable &&
              <div className='row'>
                {actionable.map((item, index) => {
                  if (!!item.url){
                    return <a key={item.url}
                       className={item.isPrimary ? "button-font button button-primary" : "button-font button"}
                       href={item.url}>{item.title}</a>
                  } else {
                    return <button
                      key={item.url}
                      className={item.isPrimary ? "button-font button button-primary" : "button-font button"}
                      onClick={item.onClick}>
                        {item.title}
                      </button>
                  } 
                }  
                )}
              </div>
            }
          </div>
        </div>
      </div>
    </section>
	)
} 

export default Section;