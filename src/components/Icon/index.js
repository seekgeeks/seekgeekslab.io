import React from 'react';

const Icon = ({icon}) => {
	const {src, alt} = icon
	return (
	  <div className="three columns">
	    <img src={src} alt={alt} />
	    <p>{alt}</p>
	  </div>
	)
}

export default Icon;
