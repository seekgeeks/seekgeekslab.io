import React, { Component } from 'react';

import Navbar from '../Navbar'
import Section from '../Section'
import Footer from '../Footer'

// import icons
import fStep1 from '../../images/fstep1.svg'
import fStep2 from '../../images/fstep2.svg'
import fStep3 from '../../images/fstep3.svg'
import fStep4 from '../../images/fstep4.svg'

export default class Partner extends Component {

  forfreelancer = {
    id: 'forfreelancer',
    center: true,
    subTitle: 'If you (IT Geek/ Agency) are looking to grow, we invite you to do so by adding incredible value with your choice of work. Partner with us today!',
    icons: [
      {
        id: 1,
        src: fStep1,
        alt: 'Sign up on our platform to get into our network'
      },{
        id: 2,
        src: fStep2,
        alt: 'Fill up your profile with all REQUIRED details'
      },{
        id: 3,
        src: fStep3,
        alt: 'Get verified (or not, maybe) for your work & skills'
      },{
        id: 4,
        src: fStep4,
        alt: 'Work with us on projects that suit your skills'
      }
    ],
    actionable: [
      {title: 'Get Started', url: '/register/', isPrimary: true},
    ]
  }

	render() {
		return (
			<div>
			<Navbar navs={localStorage.token ? 
        [
				{url: '/dashboard/apply/', name: 'My Profile'},
        {url: '/connect/', name: 'Connect'},
				{url: '/logout/', name: 'Logout'},
				] : [
        {url: '/connect/', name: 'Connect'},
        {url: '/login/', name:'Login', dashboard: true},]
      }/>
      <section id="hero" className="hero u-full-width">
        <div className="partner-image"></div>
        <div className="container centered">
          <div className="twelve columns">
            <h1 className="title">Are you a freelance geek or a tech agency? <br /> You are at the best place!</h1>
            <a className="button button-primary button-font" href="/register/">Partner with us</a>
          </div>
        </div>
      </section>
			<Section section={this.forfreelancer}/>
      <Footer />
			</div>
		);
	}
}
