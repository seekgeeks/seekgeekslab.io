import React from 'react';

import Navbar from '../Navbar'
import Section from '../Section'
import Footer from '../Footer'

const about = {
	id: 'about',
	title: 'About',
	Content: () => {
		return <div>
				<p>SeekGeeks is a new age platform enabling fully managed IT products/ services solutions with a network of verified IT geeks and agencies.</p>
			  <p>We built this company because we knew that with non-stop and ever-growing technological progress it is ultra necessary to value time, commitment, work process and quality over die hard competition on pricing and manipulated delivery mechanisms. This is why we have taken it into our hands to make things better. We are changing the way projects are managed and delivered that every responsible individual or company would wish for. </p>
			  <p>Every incredible idea came from a preeminent vision and we are one of the only few companies in the world that can help startups, SMEs, Corporates, Industries, Individual brands and variety of businesses in adapting justifiable technologies and secured work methods that enable them to enjoy complete control to foster astronomical growth with sustainability. </p>
			  <p>Address your requirements today and start a project with us to stay assured of timely deliveries, strong commitments and highest work quality in realistic budgets. </p>
				<div className="center">
					<a className="button button-primary button-font" href="/#services">Start a Project</a>
				</div>
		</div>
	}
 }

const Page = (props) => {
	return (
		<div>
			<Navbar navs={localStorage.token ? 
        [
				{url: '/dashboard/apply/', name: 'My Profile'},
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/connect/', name: 'Connect'},
				{url: '/logout/', name: 'Logout'},
				] : [
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/partner/', name: 'Work on Projects'},
        {url: '/connect/', name: 'Connect'}]
      }/>
      <br/>
			<Section section={about} />
			<Footer />
		</div>
	)
} 

export default Page;