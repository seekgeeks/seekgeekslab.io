import React, { Component } from 'react';

import './ContactForm.css'

import {createContact} from '../../API'


export default class ContactForm extends Component {
	state = {
		name: '',
		email: '',
		message: '',
		submitting: null,
		submitted: null,
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	resetForm = () => {
		this.setState({
			name: '',
			email: '',
			message: '',
			submitting: null,
		});
	}

	handleSubmit = e => {
		e.preventDefault();
		this.setState({
			submitting: true,
		})
		createContact(this.state)
			.then(response => {
				this.setState({
					submitted: true,
				})
				this.resetForm()
			})
			.catch(error => { alert(error); });
	}

	render() {
		if (this.state.submitting) {
			return (
				<div className="spinner"></div>
			)
		}

		if (this.state.submitted) {
			return (
				<div>
					<h5>We have got your message, we will get back to you soon.</h5>
				</div>
			)
		}

		return (
			<div>
			<form onSubmit={this.handleSubmit} id="contactForm">
				<div className="row">
					<div className="six columns">
						<label>Your Name</label>
						<input className="u-full-width"  type="text" value={this.state.name} name="name" onChange={this.handleChange} required/>
					</div>
					<div className="six columns">
						<label>Email</label>
						<input className="u-full-width"  type="email" value={this.state.email} name="email" onChange={this.handleChange} required/>
					</div>
				</div>
				<label>Message</label>
			    <textarea className="u-full-width" name="message" value={this.state.message} onChange={this.handleChange} required/>
				<div className="center">
				<br/>
				<input className="button-font button-primary" type="submit" value="Send"/>
				</div>
			</form>
			</div>
		);
	}
}
