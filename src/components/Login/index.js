import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom'
import isEmpty from 'lodash/isEmpty';

import { login, loggedIn } from '../../Auth'
import Navbar from '../Navbar'
import Footer from '../Footer'

export default class Login extends Component {
	state = {
		redirectToReferrer: false,
		username: '',
		password: '',
		errors: ''
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		});
	}

  handleLogin = e => {
  	e.preventDefault()
    login(this.state.username, this.state.password)
    	.then(res => this.setState({redirectToReferrer:true}))
    	.catch(error => this.setState({errors: error.response.data}))
  }

	renderErrors = () => {
		const {errors} = this.state
		return !isEmpty(errors)
			&& 'non_field_errors' in errors
			&& <span style={{color: 'red', margin: '1rem'}}>
				{errors.non_field_errors[0]}
				<br />
				</span>
	}

	renderError = (field) => {
		if (this.state.errors && field in this.state.errors) {
			return (
				<ul>
				{this.state.errors[field].map(element => <li style={{color: 'red'}}>{element}</li>)}
				</ul>
			)
		}
	}

	render() {
		const { from } = this.props.location.state || { from: { pathname: '/dashboard/apply' } }
    const { redirectToReferrer } = this.state
    if (redirectToReferrer && loggedIn()) {
      return (
        <Redirect to={from}/>
      )
    }

		return (
			<div>
			<Navbar navs={localStorage.token ? 
        [
				{url: '/dashboard/apply/', name: 'My Profile'},
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/connect/', name: 'Connect'},
				{url: '/logout/', name: 'Logout'},
				] : [
				{url: '/#howitworks', name: 'How it works'},
        {url: '/#services', name: 'Services'},
        {url: '/partner/', name: 'Work on Projects'},
        {url: '/connect/', name: 'Connect'}]
      }/>
			<br />
			<br />
			<br />
			<section>
				<div className="container center">
					<div className="row">
						<h1>Log In</h1>
						<form onSubmit={this.handleLogin} id="contactForm">
							<div className="row">
								<div className="six columns" style={Styles}>
									<label>Username</label>
									<input className="u-full-width" type="text" value={this.state.username} name="username" onChange={this.handleChange} required/>
									{this.renderError('username')}
									<label>Password</label>
									<input className="u-full-width" type="password" value={this.state.password} name="password" onChange={this.handleChange} required/>
									{this.renderError('password')}
									<Link to="/reset-password/" className="u-pull-right"><strong>Forgot Password?</strong></Link>
								</div>
							</div>
							<br />
							{this.renderErrors()}
							<br />
							<input className="button-primary" type="submit" value="Login"/>
						</form>
						<p>Doesn't have an account? <Link to="/register/">Register</Link></p>
					</div>
				</div>
				</section>
			<Footer />
			</div>
		);
	}
}

const Styles = {
	float: 'none',
	margin: '0 auto'
}
