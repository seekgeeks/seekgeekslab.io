import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import './Navbar.css'

export default class Navbar extends Component {
	state = {
		displayNav: false
	}

	onClick = () => this.setState({displayNav: !this.state.displayNav})

	render() {
		const {navs} = this.props
		const {displayNav} = this.state
	return (
		<nav className='fixed-top'>
			<div className='container'>
				<div className='u-pull-left'>
				<Link to='/'>
					<img
						alt='SeekGeeks Logo'
						className='logo'
						src={'https://res.cloudinary.com/seekgeeks/image/upload/q_auto/v1512985703/seekgeeks_hibjmp.svg'}
						></img>
				</Link>
			</div>
			<a className='icon' onClick={this.onClick}>&#9776;</a>
			<ul>
				<div className={displayNav ? 'u-pull-right mobile-view responsive' : 'u-pull-right mobile-view'} id='navs'>
				{navs && navs.map(
					(item, index) => 
						<li key={item.url}>
							<a 
								href={ item.url }
								onClick={this.onClick}
								className={item.dashboard && 'button button-primary'}>
								{ item.name }</a>
						</li>
				)}
				</div>
			</ul>
			</div>
		</nav>
	)
}
}