import axios from 'axios';
import { AuthURL, rootURL } from './API'

export const login = (username, password) => {
  return getToken(username, password)
}

export const getToken = (username, password) => {
  return axios
      .post(AuthURL, { username, password})
      .then(function(res) {
        localStorage.setItem('token', res.data.token)
        return res
      })
}

export const logout = () => delete localStorage.token

export const loggedIn = () => localStorage.token

export const registerAuthURL = `${rootURL}/api/v1/auth/register/`
